# During the hack week:

1. Each project team kick off their work on Monday - 13UTC?
2. We provide a BBB link for each team to meet and hang out as they work.
3. We provide a BBB lounge room.
4. All projects should be done by Friday

## Monday - Welcome DAY:

0. Lounge with opening session.
1. Mention logistics, etc. We will have a main pad with all information and links to team's pads.
2. Each group present their project.
3. Each group will go to their meeting place (BBB or irc or whatever).
4. There will be a "lounge" space for people to drop in during the week in case there are questions or comments, etc.

## Every day:
1. Lounge for starting the day at 13UTC for whoever wants to drop in.

## Friday - DEMO DAY:

0. Lounge for DEMO Day
1. It will be recorded to upload to Youtube later.
